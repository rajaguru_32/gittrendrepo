# Git Trending Repositories in Android

An Android App that lists the most trending repositories in Android from Github.

#### App Architecture
Based on mvvm architecture and repository pattern.

#### App Features
* Users can view the most trending repositories in Android from Github.
* Users can filter based on author, language, project description. User able to search repositories with char in search bar
* Once the data is fetched successfully from remote, it stored locally.
* Pull to refresh to fetch latest data from remote
* Loading state while fetching data from remote.
* Empty state with retry option evey first time after installed the app without internet. user open the app



#### The app includes the following main components:

* A local database Room
* A web Api service retrofit,okhttp.
* A repository that works with the database and the api service, providing a data interface.
* A ViewModel that provides data specific for the UI.
* The UI, which shows a visual representation of the data in the ViewModel.

#### App Packages

* apiinterface - contains the api classes to make api calls to FreshlyPressed server, using Retrofit.
* dao - contains the db classes to cache network data.
* Model - contains the model class for local and remote.
* View - contains classes needed to display Activity.
* utils - contains the repository class and connectivity check.


#### App Specs
* Minimum SDK 24
* MVVM Architecture
* Android Architecture Components (LiveData, Lifecycle, ViewModel, Room Persistence Library, ConstraintLayout)
* [Retrofit 2](https://square.github.io/retrofit/) for API integration.
* [Gson](https://github.com/google/gson) for serialisation.
* [Okhhtp3](https://github.com/square/okhttp) for implementing interceptor, logging and mocking web server.
* [MaterialSearchView]( https://github.com/MiguelCatalan/MaterialSearchView) for search toolbar
* [swipe](https://developer.android.com/training/swipe/add-swipe-interface) for pull to refresh.
* [DroidNet](https://github.com/JobGetabu/DroidNet) listening for network connection state and Internet connectivity