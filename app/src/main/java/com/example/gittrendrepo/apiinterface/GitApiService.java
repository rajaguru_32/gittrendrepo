package com.example.gittrendrepo.apiinterface;

import androidx.room.RawQuery;

import com.example.gittrendrepo.model.TrendDataRemoteModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GitApiService {

    @RawQuery
    @GET("repositories?since=daily")
    Call<ArrayList<TrendDataRemoteModel>> listRepos();
}
