package com.example.gittrendrepo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Util {

Context context;
    public Util(Context context){
        this.context= context;
    }
    public  boolean isOnline() {
        Log.i("TAG", "isOnline: isInternetAvailable() "+isInternetAvailable());
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected() || isInternetAvailable()) {
            return true;
        }
        return false;
    }

    public static boolean isInternetAvailable() {
        StrictMode.ThreadPolicy policy =
                new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
      /*  String aLANDING_SERVER = "https://www.google.com";
        try {
            URL url = new URL(aLANDING_SERVER);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("User-Agent", "Test");
            connection.setRequestProperty("Connection", "close");
            connection.setConnectTimeout(1000); //configurable
            connection.connect();
            LogUtils.e("tag", "Error checking server connection "+connection.getResponseCode());
            return (connection.getResponseCode() == 200);
        } catch (Exception IOException) {
            LogUtils.e("tag", "Error checking server connection ");
        }
            return false;*/
    }
}
