package com.example.gittrendrepo.utils;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.gittrendrepo.apiinterface.GitApiService;
import com.example.gittrendrepo.dao.TrendDao;
import com.example.gittrendrepo.dao.TrendRoomDatabase;
import com.example.gittrendrepo.model.TrendDataLocalModel;
import com.example.gittrendrepo.model.TrendDataRemoteModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrendRepository {

    private TrendDao trendDao;
    private LiveData<List<TrendDataLocalModel>> getTrendData;
    private LiveData<List<TrendDataLocalModel>> getTrendFilterData;

    public TrendRepository(Application application){
        TrendRoomDatabase db= TrendRoomDatabase.getTrendDatabase(application);
        trendDao= db.trendDao();
        getTrendData= trendDao.getAllTreadingRepData();
    }

    public LiveData<List<TrendDataLocalModel>>  getAlldata(){

        return getTrendData;
    }

    public void insertToDB(){

        new getDataFromGitHubRep(trendDao).execute();
    }

    public LiveData<List<TrendDataLocalModel>> searchDataFromDB(String getData){
        Log.i("TAG", "onQueryTextChange: viewmodel getdata "+getData);
       LiveData<List<TrendDataLocalModel>> listLiveData= trendDao.getSearchResultFromDB(getData);
   //    listLiveData.observe(this, new O);
     //   Log.i("TAG", "onQueryTextChange: viewmodel listLiveData "+listLiveData.);
       // getTrendFilterData=listLiveData;
        return listLiveData;
    }



    private class getDataFromGitHubRep extends AsyncTask<TrendDataLocalModel,Void,Void> {
      public TrendDao asynTask;
        public getDataFromGitHubRep(TrendDao trendDao) {
            asynTask=trendDao;
        }

        @Override
        protected Void doInBackground(TrendDataLocalModel... trendDataLocalModels) {


            Retrofit retrofit=getClientLogin();

            GitApiService apiController = retrofit.create(GitApiService.class);

            Call<ArrayList<TrendDataRemoteModel>> call = apiController.listRepos();
            call.enqueue(new retrofit2.Callback<ArrayList<TrendDataRemoteModel>>(){
                @Override
                public void onResponse(Call<ArrayList<TrendDataRemoteModel>> call, final Response<ArrayList<TrendDataRemoteModel>> response) {
                    Log.i("TAG", trendDao+" onResponse: getAuthor "+response.body().get(0).getAuthor());

                    // arrayList.get(1).get(1).getAuthor();


                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            asynTask.deleteAllRep();
                            for (int i=0; i<response.body().size();i++){
                                Log.i("TAG", trendDao+" onResponse: getAuthor data "+response.body().get(i).getAuthor());
                    /*    treadingDataModel.setAuthor(response.body().get(i).getAuthor());
                        treadingDataModel.setAvatar(response.body().get(i).getAvatar());
                        treadingDataModel.setUrl(response.body().get(i).getUrl());
                        treadingDataModel.setDescription(response.body().get(i).getDescription());
                        treadingDataModel.setLanguage(response.body().get(i).getLanguage());
                        treadingDataModel.setLanguageColor(response.body().get(i).getLanguageColor());
                        treadingDataModel.setStars(response.body().get(i).getStars());
                        treadingDataModel.setForks(response.body().get(i).getForks());
                        treadingDataModel.setCurrentPeriodStars(response.body().get(i).getCurrentPeriodStars());*/

//                        asynTask.insertTrendingRepDetails(treadingDataModel);



                                for (int j=0; j<response.body().get(i).builtBy.size(); j++){
                                    Log.i("TAG", trendDao+" onResponse: getHref "+response.body().get(i).builtBy.get(j).getHref());
                                    Log.i("TAG", trendDao+" onResponse: getAvatar "+response.body().get(i).builtBy.get(j).getAvatar());
                                    Log.i("TAG", trendDao+" onResponse: getUsername "+response.body().get(i).builtBy.get(j).getUsername());
                                    Log.i("TAG", trendDao+" onResponse: getAuthor r "+response.body().get(i).getAuthor());
                                    TrendDataLocalModel treadingDataModel= new TrendDataLocalModel();
                                    treadingDataModel.setAuthor(response.body().get(i).getAuthor());
                                    treadingDataModel.setName(response.body().get(i).getName());
                                    treadingDataModel.setAvatar(response.body().get(i).getAvatar());
                                    treadingDataModel.setUrl(response.body().get(i).getUrl());
                                    treadingDataModel.setDescription(response.body().get(i).getDescription());
                                    treadingDataModel.setLanguage(response.body().get(i).getLanguage());
                                    treadingDataModel.setLanguageColor(response.body().get(i).getLanguageColor());
                                    treadingDataModel.setStars(response.body().get(i).getStars());
                                    treadingDataModel.setForks(response.body().get(i).getForks());
                                    treadingDataModel.setCurrentPeriodStars(response.body().get(i).getCurrentPeriodStars());
                                    treadingDataModel.setHref(response.body().get(i).builtBy.get(j).getHref());
                                    treadingDataModel.setAvatarBuildTypw(response.body().get(i).builtBy.get(j).getAvatar());
                                    treadingDataModel.setUsername(response.body().get(i).builtBy.get(j).getUsername());


                                    // Insert Data
                                    //   AppDatabase.getInstance(context).userDao().insert(new User(1,"James","Mathew"));

                                    // Get Data
                                    // AppDatabase.getInstance(context).userDao().getAllUsers();

                                    asynTask.insertTrendingRepDetails(treadingDataModel);

                                    //      asynTask.insertTrendingRepDetails(treadingDataModel);

                                }


                            }
                        }
                    });
                    //response;

                    //   return arrayList;

                }

                @Override
                public void onFailure(Call<ArrayList<TrendDataRemoteModel>> call, Throwable t) {

                }
            });
/*
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://private-01934-githubtrendingapi.apiary-mock.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            GitHubService apiController = retrofit.create(GitHubService.class);
            Call<Result> call = apiController.listRepos();
            Log.i("TAG", noDos+" onResponse: "+call.toString());
            call.enqueue(new retrofit2.Callback<Result>() {

                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {

                    Log.i("TAG", noDos+" onResponse: "+response.message());

                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Log.i("TAG", noDos+" onResponse: error "+t);
                }
            });*/




            return null;




        }


        public  Retrofit retrofitLogin = null;
        public  Retrofit getClientLogin() {
            //  if (retrofitLogin == null) {
            retrofitLogin = new Retrofit.Builder()
                    .baseUrl("https://private-01934-githubtrendingapi.apiary-mock.com/")
                    .client(oktpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            //     }
            return retrofitLogin;


        }


        public  OkHttpClient oktpClient() {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();
            return client;
        }
    }
}
