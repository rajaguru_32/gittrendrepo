package com.example.gittrendrepo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TrendDataRemoteModel {

    //   @SerializedName("_OrderDetails")
    //  private OrderDetails[] mOrderDetails;
    @SerializedName("author")
    private String author;
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    @SerializedName("name")
    private String name;

    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @SerializedName("avatar")
    private String avatar;
    @SerializedName("url")
    private String url;

    @SerializedName("description")
    private String description;
    @SerializedName("language")
    private String language;

    @SerializedName("languageColor")
    private String languageColor;
   @SerializedName("stars")
    private int stars;
    @SerializedName("forks")
    private int forks;
    @SerializedName("currentPeriodStars")
    private int currentPeriodStars;
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColor) {
        this.languageColor = languageColor;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getCurrentPeriodStars() {
        return currentPeriodStars;
    }

    public void setCurrentPeriodStars(int currentPeriodStars) {
        this.currentPeriodStars = currentPeriodStars;
    }

    public ArrayList<builtByvalue> builtBy;


    public class builtByvalue
    {
        String avatar;
        String username;
        String href;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }


        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }



    }


    // Add setters and getters
/*
        public static class OrderDetails implements Serializable {
            @SerializedName("ProductName")
            private String mProductName;

            @SerializedName("TotalAfterDiscount_Lc")
            private String mTotalAfterDiscount;

            @SerializedName("MeasureUnitName")
            private String mMeasureUnitName;

            @SerializedName("TotalPrice_Lc"
                    private String mTotalPrice;

                    @SerializedName("PricePerUnit_Lc")
                    private String mPricePerUnit;

                    @SerializedName("Quantity")
                    private String mQuantity;

            // Add setters and getters
        }*/

}
