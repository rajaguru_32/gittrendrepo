package com.example.gittrendrepo.model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.gittrendrepo.dao.TrendRoomDatabase;
import com.example.gittrendrepo.utils.TrendRepository;

import java.util.List;

public class TrendViewModel extends AndroidViewModel {

    private TrendRepository trendRepository;
    private LiveData<List<TrendDataLocalModel>> getAllTrendData;

    private LiveData<List<TrendDataLocalModel>> getAllFilterDataTrendData;


    public TrendViewModel(@NonNull Application application) {
        super(application);
        trendRepository= new TrendRepository(application);
        getAllTrendData= trendRepository.getAlldata();
    }

    public void callGitHubRep(){
        trendRepository.insertToDB();
    }

    public LiveData<List<TrendDataLocalModel>> getTrenDatafromDB(){

        return getAllTrendData;
    }

    public  LiveData<List<TrendDataLocalModel>> setData(String textValue){
        Log.i("TAG", "onQueryTextChange: viewmodel "+textValue);

      trendRepository.searchDataFromDB(textValue);
      return   trendRepository.searchDataFromDB(textValue);
    }

}
