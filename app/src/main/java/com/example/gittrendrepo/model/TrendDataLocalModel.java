package com.example.gittrendrepo.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "trendData_table")
public class TrendDataLocalModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo
    private String author;

    @ColumnInfo
    private String name;
    @ColumnInfo
    private String avatar;
    @ColumnInfo
    private String url;
    @ColumnInfo
    private String description;
    @ColumnInfo
    private String language;
    @ColumnInfo
    private String languageColor;
    @ColumnInfo
    private int stars;
    @ColumnInfo
    private int forks;
    @ColumnInfo
    private int currentPeriodStars;
    @ColumnInfo
    private String avatarBuildTypw;
    @ColumnInfo
    private String username;
    @ColumnInfo
    private String href;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColor) {
        this.languageColor = languageColor;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getCurrentPeriodStars() {
        return currentPeriodStars;
    }

    public void setCurrentPeriodStars(int currentPeriodStars) {
        this.currentPeriodStars = currentPeriodStars;
    }

    public String getAvatarBuildTypw() {
        return avatarBuildTypw;
    }

    public void setAvatarBuildTypw(String avatarBuildTypw) {
        this.avatarBuildTypw = avatarBuildTypw;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
