package com.example.gittrendrepo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.example.gittrendrepo.R;
import com.example.gittrendrepo.databinding.ActivityHomeBinding;
import com.example.gittrendrepo.model.TrendDataLocalModel;
import com.example.gittrendrepo.model.TrendViewModel;
import com.example.gittrendrepo.utils.Util;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, DroidListener {

    private TrendViewAdapter trendListAdapter;
    private TrendViewModel trendViewModel;
    MaterialSearchView searchView;
    List<TrendDataLocalModel> trendingDataAllDetails;
    ProgressBar progressBar;
    Util util;
    TextView noNetwork;
    Button noNetworkBtn;
    ActivityHomeBinding binding;
    private DroidNet mDroidNet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        trendViewModel=new ViewModelProvider(this).get(TrendViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
         searchView = (MaterialSearchView) findViewById(R.id.search_view);
         progressBar= findViewById(R.id.progressBar);
        noNetwork =  findViewById(R.id.no_network);
        noNetworkBtn =  findViewById(R.id.no_network_btn);
        noNetworkBtn.setOnClickListener(this);
         util=new Util(this);

        DroidNet.init(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener( HomeActivity.this);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);

        int swiperefreshSetBGColor = getResources().getColor(R.color.colorPrimary, getTheme());
        int swiperefreshSetColor = getResources().getColor(R.color.colorPrimaryDark,getTheme());
        binding.swiperefresh.setColorSchemeColors(swiperefreshSetColor);
        binding.swiperefresh.setProgressBackgroundColorSchemeColor(swiperefreshSetBGColor);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        trendListAdapter = new TrendViewAdapter(this);

        Log.i("TAG", "onCreate: "+util.isOnline());

        //set adapter
        recyclerView.setAdapter(trendListAdapter);
        //divider
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.recyclerview.getContext(),
                new LinearLayoutManager(this).getOrientation());

        Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.horizontal_divider);
        dividerItemDecoration.setDrawable(horizontalDivider);
        binding.recyclerview.addItemDecoration(dividerItemDecoration);

        // get data from Local DB
        getdataFromViewModel();
        // filter data from Local db
        searchListtner();
        //swipe listener
        swipeListener();

        // check connectivity's
        if (util.isOnline()) {
            //gets data from remote
            trendViewModel.callGitHubRep();
            noNetwork.setVisibility(View.GONE);
            noNetworkBtn.setVisibility(View.GONE);
        }else {
            progressBar.setVisibility(View.GONE);
        //    noNetwork.setVisibility(View.VISIBLE);
          //  noNetworkBtn.setVisibility(View.VISIBLE);
        }
    }

    private void getdataFromViewModel() {
        trendViewModel.getTrenDatafromDB().observe(this, new Observer<List<TrendDataLocalModel>>() {
            @Override
            public void onChanged(List<TrendDataLocalModel> trendingDataTables) {

                if (trendingDataTables.size()<=0){
                    if (util.isOnline()) {
                        progressBar.setVisibility(View.VISIBLE);
                        binding.recyclerview.setVisibility(View.GONE);
                    }
                    else   { progressBar.setVisibility(View.GONE);
                        noNetwork.setVisibility(View.VISIBLE);
                        noNetworkBtn.setVisibility(View.VISIBLE);
                        binding.recyclerview.setVisibility(View.GONE);
                    }

                }else {

                    binding.recyclerview.setVisibility(View.VISIBLE);

                    Log.i("TAG", trendingDataTables.size() + " onChanged: " + trendingDataTables.size());
                    trendListAdapter.setGitRep(trendingDataTables);
                    trendingDataAllDetails = trendingDataTables;
                    progressBar.setVisibility(View.GONE);
                    noNetwork.setVisibility(View.GONE);
                    noNetworkBtn.setVisibility(View.GONE);
                    binding.swiperefresh.setRefreshing(false);
                }
                //    noDoListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void swipeListener() {
        binding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i("LOG_TAG", "onRefresh called from SwipeRefreshLayout");
                if (util.isOnline()) {
                    trendViewModel.callGitHubRep();
                }else {
                    binding.swiperefresh.setRefreshing(false);

                }

            }
        });
    }


    public void searchListtner(){

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                Log.i("TAG", "onQueryTextChange: textsubmit "+query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                Log.i("TAG", "onQueryTextChange: "+newText);
                trendViewModel.setData("%"+newText+"%").observe(HomeActivity.this, new Observer<List<TrendDataLocalModel>>() {
                    @Override
                    public void onChanged(List<TrendDataLocalModel> trendDataLocalModels) {
                       if (trendDataLocalModels.size()>0) {
                            Log.i("TAG", "onQueryTextChange: trendDataLocalModels " + trendDataLocalModels.get(0).getAuthor());

                           trendListAdapter.setGitRep(trendDataLocalModels);
                       }
                        }
                });
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
                Log.i("TAG", "onSearchViewShown: ");
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
                trendListAdapter.setGitRep(trendingDataAllDetails);
                Log.i("TAG", "onSearchViewClosed: ");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
       MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);


        return true;
    }



    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            trendListAdapter.setGitRep(trendingDataAllDetails);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        if (v==findViewById(R.id.no_network_btn)){
            Log.i("TAG", "onClick: hit git api");
            trendViewModel.callGitHubRep();
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected ) {
            //do Stuff with internet
         trendViewModel.callGitHubRep();
        }
    }
}