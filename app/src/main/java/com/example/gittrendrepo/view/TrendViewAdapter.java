package com.example.gittrendrepo.view;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gittrendrepo.R;
import com.example.gittrendrepo.model.TrendDataLocalModel;

import java.util.List;

public class TrendViewAdapter extends RecyclerView.Adapter<TrendViewAdapter.TrendViewHolder> {

    private  LayoutInflater trendInflater;
   // private List<NoDo> noDoList; //cached copy of nodo items
    private List<TrendDataLocalModel> trendingDataTables;

    Context context;
    public TrendViewAdapter(Context context){

            trendInflater = LayoutInflater.from(context);
            this.context=context;
    }

    @NonNull
    @Override
    public TrendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = trendInflater.inflate(R.layout.recyclertrendview_item, parent, false);

        return new TrendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrendViewHolder holder, int position) {

        Log.i("TAG", "onBindViewHolder: "+trendingDataTables.size());

        holder.author.setText(trendingDataTables.get(position).getAuthor());
        if (trendingDataTables != null) {
            TrendDataLocalModel current = trendingDataTables.get(position);

            holder.author.setText(current.getAuthor());
            holder.nameOfTheProject.setText(current.getName());
            holder.projDescrption.setText(current.getDescription());
            holder.language.setText(current.getLanguage());
            Log.i("TAG", current.getName()+ " onBindViewHolder: "+current.getStars());
           holder.startPoints.setText(String.valueOf(current.getStars()));
           // holder.languageColor.setBackground();
            holder.languageColor.setBackgroundColor(Color.parseColor(current.getLanguageColor()));;
        } else {

          //  holder.author.setText(R.string.no_notodo);
        }
    }

    public void setGitRep(List<TrendDataLocalModel> trendingDataTab){
        trendingDataTables = trendingDataTab;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        if (trendingDataTables != null)
            return trendingDataTables.size();
        else return 0;
    }

    public class TrendViewHolder extends RecyclerView.ViewHolder {
    public TextView author, nameOfTheProject,projDescrption, language,startPoints;
    public ImageView languageColor;

    public TrendViewHolder(@NonNull View itemView) {
        super(itemView);
        author = itemView.findViewById(R.id.author);
        nameOfTheProject = itemView.findViewById(R.id.nameofproject);
        projDescrption = itemView.findViewById(R.id.projectdescro);
        language = itemView.findViewById(R.id.language);
        languageColor= itemView.findViewById(R.id.language_color);
        startPoints = itemView.findViewById(R.id.start_value);
    }

}


}
