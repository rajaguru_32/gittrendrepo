package com.example.gittrendrepo.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.gittrendrepo.model.TrendDataLocalModel;

@Database(entities = {TrendDataLocalModel.class}, version = 1)
public abstract class TrendRoomDatabase extends RoomDatabase {
public static volatile TrendRoomDatabase INSTANCE;
public abstract TrendDao trendDao();

public static TrendRoomDatabase getTrendDatabase(final Context context){
    if (INSTANCE ==null){
        synchronized (TrendRoomDatabase.class){
            INSTANCE= Room.databaseBuilder(context.getApplicationContext(),TrendRoomDatabase.class,"trend_database").build();
        }
    }

    return INSTANCE;
}


}
