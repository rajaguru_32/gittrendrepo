package com.example.gittrendrepo.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.gittrendrepo.model.TrendDataLocalModel;

import java.util.List;

@Dao
public interface TrendDao {

   @Insert
    void insertTrendingRepDetails(TrendDataLocalModel trendDataLocalModel);

    @Query("SELECT * FROM trendData_table")
    LiveData<List<TrendDataLocalModel>> getAllTreadingRepData();


    @Query("DELETE FROM trendData_table")
    void deleteAllRep();

  // @Query("SELECT * FROM trendData_table WHERE (:value)")

   @Query("SELECT * FROM trendData_table WHERE author LIKE :value OR name LIKE :value OR language LIKE :value OR description LIKE :value")
   LiveData<List<TrendDataLocalModel>>  getSearchResultFromDB(String value);
}
